my_dict = {'a': 1, 'b': 2}
dict = {'c': 3, 'd': 4}
d = my_dict

for k, v in dict.items():
    if k in my_dict:
        d[k] += v
    else:
        d.update({k: v})
print(d)

