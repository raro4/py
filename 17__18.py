class Cart:
    def __init__(self):
        self.goods = []  # список объектов для покупки

    def add(self, gd):  # добавление в корзину товара, представленного объектом gd
        self.goods.append(gd)

    def remove(self, indx):  # удаление из корзины товара по индексу indx
        self.goods.pop(indx)

    def get_list(self):
        return [f'{x.name}: {x.price}' for x in self.goods]


class Table:
    def __init__(self, name, price):
        self.name = name
        self.price = price


class TV:
    def __init__(self, name, price):
        self.name = name
        self.price = price


class Notebook:
    def __init__(self, name, price):
        self.name = name
        self.price = price


class Cup:
    def __init__(self, name, price):
        self.name = name
        self.price = price


cart = Cart()
cart.add(TV("samsung", 10368))
cart.add(TV("Nokia", 9019))
cart.add(Table("ikea", 3000))
cart.add(Notebook("HP", 20000))
cart.add(Cup("cup of tea", 800))
