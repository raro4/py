def decorator(func):
    def inner(*args, **kwargs):
        inner.sch += 1
        return func(*args, **kwargs)

    inner.sch = 0
    return inner


@decorator
def f():
    return 1


f()
f()
print(f.sch)
