def sum_range(a, z):
    total = 1
    for i in range(a, z + 1):
        total *= i
    return total


print(sum_range(1, 5))
