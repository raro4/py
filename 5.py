my_dict = {
    'a': 10,
    'b': 52,
    'c': 14,
    'd': 34,
    'e': 79,
    'f': 6
}

res = sorted(my_dict, key=my_dict.get, reverse=True)[:3]
print(res)